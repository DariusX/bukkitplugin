package com.practicegoodtheory.common;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ColorUtils
{

  /**
   * @param args
   * @throws IOException
   */
  public static Color[][] getImagePixelMatrix(String fileName) throws IOException
  {

    BufferedImage imgBuffer = ImageIO.read(new File(fileName));

    int w = imgBuffer.getWidth();
    int h = imgBuffer.getHeight();

    Color[][] colormMatrix = new Color[w][h];
    for (int i = 0; i < w; i++)
    {
      for (int j = 0; j < h; j++)
      {
        colormMatrix[i][j] = new Color(imgBuffer.getRGB(i, j));
      }
    }

    return colormMatrix;
  }

  /**
   * Compute square of the distance between two colors
   * Treats red, green and blue as three axes -- x, y, z -- and each color is a point in this 3-D space
   * @param color1
   * @param othColor
   * @return
   */
  public static double distance2(Color color1, Color othColor)
  {
    double dr2 = Math.pow((color1.getRed() - othColor.getRed()), 2);
    double dg2 = Math.pow((color1.getGreen() - othColor.getGreen()), 2);
    double db2 = Math.pow((color1.getBlue() - othColor.getBlue()), 2);  
    return dr2 + dg2 + db2;
  }
  
}
