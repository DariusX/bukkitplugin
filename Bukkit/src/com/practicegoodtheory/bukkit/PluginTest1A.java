package com.practicegoodtheory.bukkit;

import java.awt.Color;
import java.io.File;
import java.io.IOException;

import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.material.Wool;
import org.bukkit.plugin.java.JavaPlugin;

import com.practicegoodtheory.common.ColorUtils;

public class PluginTest1A extends JavaPlugin
{

  @Override
  public void onEnable()
  {
    getLogger().info(
      "onEnable  ------------ TEST1A ------------- has been invoked!");
  }

  @Override
  public void onDisable()
  {
    getLogger().info(
      "onDisable ------------ TEST1A ------------- has been invoked!");
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
  {
    
    if (cmd.getName().equalsIgnoreCase(
      "test"))
    {
      if (!(sender instanceof Player))
      {
        sender.sendMessage("This command can only be run by a player.");
      }
      else
      {
        Player player = (Player) sender;
        ItemStack[][] tmpItemStacks = null;
        try
        {
          tmpItemStacks = readItemStacksFromImage("./plugins/draw_default_image.jpg");
        }
        catch (Exception e)
        {
          getLogger().info(
            "Could not read image. " + e.getMessage());
          return false;
        }

        Location loc = player.getLocation().clone();
        drawPlane(
          loc,
          tmpItemStacks);
      }
      return true;
    } 

    return false;
  }


  private void drawPlane(Location loc, ItemStack[][] itemStacks)
  {
    int xLoc = loc.getBlockX() + 2;
    loc.setX(xLoc);
    loc.setZ(loc.getBlockZ() + 2);
    getLogger().info("setting");
    for (int zIdx = 0; zIdx < itemStacks.length; zIdx++)
    {
      BlockBrush brush = BlockBrush.createBlockBrush(getLogger(), loc, itemStacks[zIdx], 1);
      brush.on();
      getLogger().info("Starting draw zIdx="+zIdx);
      brush.moveX(itemStacks.length);
      loc.setZ(loc.getBlockZ() + 1);
      loc.setX(xLoc);
    }

  }

  private void drawSquare( Location loc, ItemStack[] itemStacks)
  {
    loc.setX(loc.getBlockX() - 3);
    loc.setZ(loc.getBlockZ() - 3);
    getLogger().info("setting");
    BlockBrush brush = BlockBrush.createBlockBrush(getLogger(), loc, itemStacks, 1);
    brush.on();
    getLogger().info("Starting draw");
    brush.moveX(7);
    getLogger().info("finished draw");
    ItemStack wool = new ItemStack(Material.WOOL);
    wool.setData(new Wool(DyeColor.PURPLE));
    brush.setItemStacks(new ItemStack [] {wool});
    brush.moveZ(7);
    
    brush.setItemStacks(new ItemStack [] {new ItemStack(Material.DIAMOND_BLOCK)});
    brush.moveX(-7);
    
    brush.setItemStacks(new ItemStack [] {new ItemStack(Material.GLASS)});
    brush.moveZ(-7);
  }

  private void addToPlayerInventory(Player player)
  {
    PlayerInventory inventory = player.getInventory(); // The player's
                                                       // inventory
    ItemStack itemstack = new ItemStack(Material.GLASS,
      64); // A stack of diamonds

    if (inventory.contains(itemstack))
    {
      inventory.addItem(itemstack); // Adds a stack of diamonds to the
                                    // player's inventory
      player
        .sendMessage("Welcome! You seem to be reeeally rich, so we gave you some more diamonds!");
    }
  }

  
  public static ItemStack [][] readItemStacksFromImage (String filePath)
  {
    try
    {
      File current = new File(filePath);
      if (!current.exists())
      {
        throw new IllegalArgumentException("File does not exist!"+current.getAbsolutePath());
      }
      Color [][] pixelArray2D = ColorUtils.getImagePixelMatrix(current.getPath());
      ItemStack [][] itemStacks = ColorMaterialFinder.getClosestMaterials(pixelArray2D);
      if (itemStacks != null)
      {
        return itemStacks;
      }

    }
    catch (IOException e)
    {
      throw new IllegalArgumentException("IOException Reading image file. "+e.getMessage());
    }
    return null;
  }
  
  void createXZPlane(Location loc, int xLength, int zLength, ItemStack mat)
  {
    loc = loc.clone();
    for (int i = 0; i < xLength; i++)
    {
      createZRow(
        loc,
        zLength,
        mat);
      loc.setX(loc.getBlockX() + 1);
    }
  }
  
  void createYZPlane(Location loc, int yLength, int zLength, ItemStack mat)
  {
    loc = loc.clone();
    for (int i = 0; i < yLength; i++)
    {
      createZRow(
        loc,
        zLength,
        mat);
      loc.setY(loc.getBlockY() + 1);
    }
  }

  void createZRow(Location loc, int length, ItemStack mat)
  {
    loc = loc.clone();
    BlockBrush brush = BlockBrush.createBlockBrush(getLogger(), loc, new ItemStack [] {mat}, 1);
    brush.on();
    brush.moveZ(length);
  }

  void createXYRows(Location loc, int xLength, int yLength, ItemStack mat)
  {
    loc = loc.clone();
    for (int i = 0; i < yLength; i++)
    {
      createXRow(
        loc,
        xLength,
        mat);
      loc.setY(loc.getBlockY() + 1);
    }
  }

  void createXRow(Location loc, int length, ItemStack mat)
  {
    loc = loc.clone();
    BlockBrush brush = BlockBrush.createBlockBrush(getLogger(), loc, new ItemStack [] {mat}, 1);
    brush.on();
    brush.moveX(length);
  }
  
  void createYRow(Location loc, int length, Material mat)
  {

    loc = loc.clone();
    loc.setY(loc.getBlockY() - Math.floor(length / 2.0));
    for (int i = 0; i < length; i++)
    {

      // getLogger().info( "i="+i+", X="+loc.getX());

      Block theBlock = loc.getBlock();
      theBlock.setType(mat);
      loc.setY(loc.getY() + 1);
    }
  }

}
