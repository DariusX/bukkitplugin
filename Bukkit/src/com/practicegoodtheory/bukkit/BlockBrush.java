package com.practicegoodtheory.bukkit;

import java.util.logging.Logger;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;

/**
 * 
 * <p>BlockBrush</p>
 * <p>Objects of this class act like LOGO "turtles", using a painting metaphor.
 * They can be assigned a Type of material to create blocks (analogous to a paint color)
 * They can be turned on and off
 * They can be moved along X, Y and Z axes
 * If turned on, they create blocks (i.e. "paint") while moving
 * </p>
 * <p>Copyright: Copyright (c) 2013 - Creative Commons Attribution </p>
 * @author practiceGoodTheory.com
 */
public class BlockBrush
{

  private Location loc;
  private ItemStack [] itemStacks;
  private int itmIdx;
  private boolean isOn;
  private int thickness = 1;
  private Logger logger;
  
  /**
   * Creates a BlockBrush, positioned at a particular location, and with at
   * least one ItemStack
   * 
   * @param logger
   * @param initLocation Initial location of the BlockBrush
   * @param itemStacks Can be a 1-entry array if the BlockBrush is to create a
   *          single type of block. Can have a length greater than 1, in which
   *          case, the BlockBrush will cycle through the ItemStacks. For
   *          example, setting it up with three types (say, Dirt, Stone and
   *          Glass) and drawing a 9-block line, will create blocks: Dirt,
   *          Stone, Glass, Dirt, Stone, Glass, Dirt, Stone Glass
   * 
   * @return
   */
  public static BlockBrush createBlockBrush(Logger logger, Location initLocation, ItemStack [] itemStacks, int thickness)
  {
    if (initLocation == null)
    {
      throw new IllegalArgumentException("initial location cannot be null");
    }

    BlockBrush tmpBlockBrush = new BlockBrush();
    tmpBlockBrush.loc = initLocation.clone();
    tmpBlockBrush.setItemStacks(itemStacks);
    tmpBlockBrush.logger = logger; 
    tmpBlockBrush.thickness = thickness;
    
    return tmpBlockBrush;
  }
  
  private BlockBrush()
  {
  }
  
  public void setItemStacks(ItemStack[] itemStacks)
  {

    if (itemStacks == null)
    {
      throw new IllegalArgumentException("ItemStacks array cannot be null");
    }
    if (itemStacks.length == 0)
    {
      throw new IllegalArgumentException("ItemStacks array cannot have zero-length");
    }
    for (ItemStack nxtItmStack : itemStacks)
    {
      if (nxtItmStack == null)
      {
        throw new IllegalArgumentException("ItemStacks array cannotcontain null entries");
      }
    }
    
    this.itemStacks = itemStacks;
    itmIdx = 0;
  }
  
  
  public BlockBrush on()
  {
    this.isOn = true;
    return this;
  }
  
  public BlockBrush off ()
  {
    this.isOn = false;
    return this;
  }
  
  /**
   * Moves, with or without drawing, the BlockBrush by dx units (positive or negative) along the x-axis. 
   * Whether it draws or not, depends on whether it has been set to on() or off()
   * 
   * @param dx Movement in the X direction (aka, Delta-X). Zero implies no movement. 
   * @return The BlockBrush itself (allows of easy "nesting" of calls)
   */
  public BlockBrush moveX(double dx)
  {
    if (this.isOn)
    {
      if (dx > 0)
      {
        for (int i = 0; i < dx; i++)
        {
          dot();
          loc.setX(loc.getX() + 1);
        }
      }
      else
      {
        for (int i = 0; i > dx; i--)
        {
          dot();
          loc.setX(loc.getX() - 1);
        } 
      }
    }
    else
    {
      loc.setX(loc.getX() + dx);
    }
    return this;
  }
  
  public BlockBrush moveY(double dy)
  {
    if (this.isOn)
    {
      if (dy > 0)
      {
        for (int i = 0; i < dy; i++)
        {
          dot();
          loc.setY(loc.getY() + 1);
        }
      }
      else
      {
        for (int i = 0; i > dy; i--)
        {
          dot();
          loc.setY(loc.getY() - 1);
        }        
      }
    }
    else
    {
      loc.setY(loc.getY() + dy);
    }
    return this;
  }
  
  public BlockBrush moveZ(double dz)
  {
    if (this.isOn)
    {
      if (dz > 0)
      {
        for (int i = 0; i < dz; i++)
        {
          dot();
          loc.setZ(loc.getZ() + 1);
        }
      }
      else
      {
        for (int i = 0; i > dz; i--)
        {
          dot();
          loc.setZ(loc.getZ() - 1);
        }
      }
    }
    else
    {
      loc.setZ(loc.getZ() + dz);
    }
    return this;
  }
  
  private void dot()
  {
    //TODO: Place the  block
    //logger.info("dot itmIdx="+itmIdx);
    loc.getBlock().setType(itemStacks[itmIdx].getType());
    if (itemStacks[itmIdx].getData() instanceof Wool)
    {
      //TODO: using deprecated technique
      Wool x = (Wool) itemStacks[itmIdx].getData();
      loc.getBlock().setData((byte) x.getColor().ordinal());
    }

    //Increment to the next material
    itmIdx++;
    if (itmIdx >= itemStacks.length)
    {
      itmIdx = 0;
    }
    
  }




}
