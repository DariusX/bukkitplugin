package com.practicegoodtheory.bukkit;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;

import com.practicegoodtheory.common.ColorUtils;

public class ColorMaterialFinder
{
  private static List<ColorMaterialPair> colorList = new ArrayList<ColorMaterialPair>();
  static 
  {
    colorList.add(new ColorMaterialPair(new Color(145, 145, 145), new ItemStack(Material.CLAY)));
    colorList.add(new ColorMaterialPair(new Color(119, 68, 53), new ItemStack(Material.CLAY_BRICK)));
    colorList.add(new ColorMaterialPair(new Color(3, 3, 3), new ItemStack(Material.COAL_BLOCK)));
    colorList.add(new ColorMaterialPair(new Color(44, 44, 44), new ItemStack(Material.COBBLESTONE)));
    colorList.add(new ColorMaterialPair(new Color(74, 203, 197), new ItemStack(Material.DIAMOND_BLOCK)));
    colorList.add(new ColorMaterialPair(new Color(200, 200, 200), new ItemStack(Material.IRON_BLOCK)));
    colorList.add(new ColorMaterialPair(new Color(182, 109, 18), new ItemStack(Material.JACK_O_LANTERN)));
    colorList.add(new ColorMaterialPair(new Color(42, 65, 111), new ItemStack(Material.LAPIS_BLOCK)));
    colorList.add(new ColorMaterialPair(new Color(83, 109, 83), new ItemStack(Material.MOSSY_COBBLESTONE)));
    colorList.add(new ColorMaterialPair(new Color(6, 6, 6), new ItemStack(Material.OBSIDIAN)));
    colorList.add(new ColorMaterialPair(new Color(166, 95, 13), new ItemStack(Material.PUMPKIN)));
    colorList.add(new ColorMaterialPair(new Color(205, 197, 150), new ItemStack(Material.SANDSTONE)));
    colorList.add(new ColorMaterialPair(new Color(50, 50, 50), new ItemStack(Material.STONE)));
    colorList.add(new ColorMaterialPair(new Color(146, 103, 71), new ItemStack(Material.DIRT)));
    colorList.add(new ColorMaterialPair(new Color(210, 210, 210), new ItemStack(Material.GLASS)));
    
    colorList.add(new ColorMaterialPair(new Color(254, 254, 254), new Wool(DyeColor.WHITE)));
    colorList.add(new ColorMaterialPair(new Color(219, 123, 59), new Wool(DyeColor.ORANGE)));
    colorList.add(new ColorMaterialPair(new Color(166, 63, 175), new Wool(DyeColor.MAGENTA)));
    colorList.add(new ColorMaterialPair(new Color(93, 127, 197), new Wool(DyeColor.LIGHT_BLUE)));
    colorList.add(new ColorMaterialPair(new Color(188, 176, 41), new Wool(DyeColor.YELLOW)));
    colorList.add(new ColorMaterialPair(new Color(63, 170, 54), new Wool(DyeColor.LIME)));
    colorList.add(new ColorMaterialPair(new Color(216, 154, 170), new Wool(DyeColor.PINK)));
    colorList.add(new ColorMaterialPair(new Color(68, 68, 68), new Wool(DyeColor.GRAY)));
    colorList.add(new ColorMaterialPair(new Color(170, 170, 170), new Wool(DyeColor.SILVER)));
    colorList.add(new ColorMaterialPair(new Color(51, 120, 149), new Wool(DyeColor.CYAN)));
    colorList.add(new ColorMaterialPair(new Color(145, 82, 198), new Wool(DyeColor.PURPLE)));
    colorList.add(new ColorMaterialPair(new Color(49, 62, 154), new Wool(DyeColor.BLUE)));
    colorList.add(new ColorMaterialPair(new Color(86, 56, 34), new Wool(DyeColor.BROWN)));
    colorList.add(new ColorMaterialPair(new Color(55, 74, 29), new Wool(DyeColor.GREEN)));
    colorList.add(new ColorMaterialPair(new Color(174, 62, 56), new Wool(DyeColor.RED)));
    colorList.add(new ColorMaterialPair(new Color(12, 12, 12), new Wool(DyeColor.BLACK)));

  }
  
  public static ItemStack[][] getClosestMaterials(Color[][] colors2D)
  {
    if (colors2D == null || colors2D.length == 0)
    {
      throw new IllegalArgumentException("Colors[][] is empty");
    }
    ItemStack[][] closestMaterials = new ItemStack[colors2D.length][];
    for (int i = 0; i < colors2D.length; i++)
    {
      closestMaterials[i] = getClosestMaterials(colors2D[i]);
    }
    return closestMaterials;
  }
  
  
  public static ItemStack[] getClosestMaterials(Color[] colors1D)
  {
    if (colors1D == null || colors1D.length == 0)
    {
      throw new IllegalArgumentException("Colors[] is empty");
    }
    ItemStack[] closestMaterials = new ItemStack[colors1D.length];
    for (int i = 0; i < colors1D.length; i++)
    {
      closestMaterials[i] = getClosestMaterial(colors1D[i]);
    }
    return closestMaterials;
  }
  
  public static ItemStack getClosestMaterial(Color findColor)
  {
    ItemStack closestMaterial = null;
    double minDistance = Double.MAX_VALUE;
    for (ColorMaterialPair nxtPair : colorList)
    {
      double nxtDistance = ColorUtils.distance2(nxtPair.color, findColor);
      if (nxtDistance < minDistance)
      {
        minDistance = nxtDistance;
        closestMaterial = nxtPair.itemStack;
      }
    }
    return closestMaterial;
  }
  
  private static class ColorMaterialPair 
  {
    private Color color;
    private ItemStack itemStack;
    
    public ColorMaterialPair(Color color,  ItemStack itemStack)
    {
       this.color = color;
       this.itemStack = itemStack;
    }   
    
    public ColorMaterialPair(Color color,  Wool wool)
    {
       this.color = color;
       this.itemStack = wool.toItemStack();
    }   
    
  }
}
