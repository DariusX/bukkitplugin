This project contains classes to be used with the Bukkit plugin-API for Minecraft

Licence: 

(c) Copyright 2013, practicegoodTheory.com

Licence: Creative Commons Attribution -- This license lets others distribute, remix, 
tweak, and build upon your work, even commercially, as long as they credit you for
 the original creation. See details here; http://creativecommons.org/licenses/by/4.0/
 
 PluginTest1A is a Bukkit plugin that reads a graphic file and draws a similar image 
 in Minecraft, using a block for each pixel.